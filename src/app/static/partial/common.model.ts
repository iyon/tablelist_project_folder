export interface AgHeaders{
  headerName: string;
  field?: string;
  width?: number;
  rowGroupIndex?: number;
  children?: AgHeaders[]
}


