import {Component} from "@angular/core";
import {ICellRendererAngularComp} from "ag-grid-angular";
import { Http, ResponseContentType } from '@angular/http';
import { Router } from '@angular/router';
import { HttpClient } from "@angular/common/http";
import { environment as env } from '@env/environment';
import { ROUTE_ANIMATIONS_ELEMENTS } from '@app/core';
import 'ag-grid-enterprise';
import { FeatureService } from '../provider/feature-service';
import { Store } from '@ngrx/store';
import { map, catchError } from 'rxjs/operators';


@Component({
    selector: 'child-cell',
    template: `
    <span>
        <!--<button style="height: 35px" [disabled]="disabled"-->
        <button style="height: 35px"
            (click)="dummyPdf()" class="btn btn-info">
            <i class="fa fa-file-pdf-o" style="    position: absolute;
            left: 40px;
            line-height: inherit;">
            </i>
                Download PDF
        </button>
    </span>`,
    styles: [
        `.btn {
            line-height: 0.5
        }`
    ]
})
export class ChildMessageRenderer implements ICellRendererAngularComp {
    public params: any;
    disabled=false;

    agInit(params: any): void {
        this.params = params;
        // if(params.data.aprstatus == "Approved"){
        //     this.disabled = true;
        // }
    }

    public invokeParentMethod() {
        // this.params.context.componentParent.methodFromParent(`Row: ${this.params.node.rowIndex}, Col: ${this.params.colDef.headerName}`)
    // alert("tes")                
        // console.log("params", this.params.data)
    // this.params.data
    }

    refresh(): boolean {
        return false;
    }


constructor(
    private http: HttpClient,
    private router: Router,
  ) { }
  
//   downloadFile() {
//     return this.http
//       .get('https://www.jica.go.jp/project/indonesian/indonesia/008/news/general/c8h0vm000043j61g-att/130121_01.pdf', {
//       })
//       .pipe(
//           map(res => {
//         return {
//           filename: 'filename.pdf',
//           data: res
//         };
//       }))
//       .subscribe(res => {
//           console.log('start download:',res);
//           var url = window.URL.createObjectURL(res.data);
//           var a = document.createElement('a');
//           document.body.appendChild(a);
//           a.setAttribute('style', 'display: none');
//           a.href = url;
//           a.download = res.filename;
//           a.click();
//           window.URL.revokeObjectURL(url);
//           a.remove(); // remove the element
//         }, error => {
//           console.log('download error:', JSON.stringify(error));
//         }, () => {
//           console.log('Completed file download.')
//         });
//   }

dummyPdf(){
    this.router.navigateByUrl('/nodin')
}
}