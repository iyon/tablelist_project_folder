import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment as env } from '@env/environment';
import { ROUTE_ANIMATIONS_ELEMENTS } from '@app/core';
import 'ag-grid-enterprise';
import { FeatureService } from '../provider/feature-service';
import { Store } from '@ngrx/store';
import { ChildMessageRenderer } from "./child-message-renderer.component";
export interface FileUpload{
  name: string,
  file: File
}
export const statuss = [];
export interface UpdateStat{
  id: number,
  name: string
}
export const newRoww = [];
export interface UpdateRow{
  field: string,
  value: string
}


@Component({
  selector: 'ag-table',
  template: `
    <ag-grid-angular
    #agGrid
    style="width: 100%;height: 385px;margin-top: 5%;"
    id="myGrid"
    gridOptions.rowHeight = 50
    [rowData]="rowData"
    class="ag-theme-material"
    [columnDefs]="columnDefs"
    [frameworkComponents]="frameworkComponents"
    [enableColResize]="true"
    [enableSorting]="true"
    [animateRows]="true"
    [multiSortKey]="multiSortKey"
    [sortingOrder]="sortingOrder"
    [masterDetail]="true"
    (gridReady)="onGridReady($event)"
    [defaultColDef]="defaultColDef"
    [rowSelection]="rowSelection"
    [suppressMenuHide]="true"
    [isRowSelectable]="isRowSelectable"
    [enableFilter]="true"
    [pagination]="true" 
    [detailCellRendererParams]="detailCellRendererParams"
    
    
  >
  </ag-grid-angular>

<!-- [paginationAutoPageSize]="true" 
[rowDeselection]="true"
[suppressRowClickSelection]="true"
-->

  `,
})



export class AgTable implements OnInit {
routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
versions = env.versions;
columnDefs;
defaultColDef;
rowData;
frameworkComponents;
sortingOrder;
multiSortKey;
gridApi;
gridColumnApi;
detailCellRendererParams;
rowSelection;
isRowSelectable;
context;
files: FileUpload[] = [];
@ViewChild('myInput1')
myInputVariable1: any;
refresh : boolean;
listStatus: any;
token: any;



  constructor(private http: HttpClient, private featureService: FeatureService, private store: Store<any>) {
    
    this.columnDefs = [
      { 
        headerName: "NO",
        field: "no",
        width: 165,
      },
      { 
        headerName: "TITLE",
        field: "title",
        width: 165,
      },
      { 
        headerName: "INPUT DATE",
        field: "input_date",
        width: 110, 
      },
      {
        headerName: "SUBMITTED BY",
        field: "submit_by",
        width: 100,
      },
      {
        headerName: "APPROVAL STATUS",
        field: "aprstatus",
        width: 100,
      },  
      {
        headerName: "DOWNLOAD NODIN",
        field: "nodin",
        cellRenderer: "childMessageRenderer",
        // colId: "params",
        width: 180
      }     
    ];
    
  this.rowSelection = "single";
  this.frameworkComponents = {
    childMessageRenderer: ChildMessageRenderer
  };
  }


onGridReady(params) {
  this.gridApi = params.api;
  this.gridColumnApi = params.columnApi;
  params.api.sizeColumnsToFit();
  this.http
    // SERVER BENER
    .get(
        "http://10.54.36.49/api-btsonair/public/api/table_list_nodin?token=" + this.token
      )
      // .get(
      //   "http://10.54.36.49/api-btsonair/public/api/v1/nodeb?token=" + this.token
      // )
    .subscribe(data => {
      this.rowData = data;
    });

  setTimeout(function() {
    var rowCount = 0;
    params.api.forEachNode(function(node) {
      node.setExpanded(rowCount++ === 1);
    });
  }, 500); 
}


  ngOnInit() {
    this.token = sessionStorage.getItem("token")
    if (sessionStorage.getItem('token')) {
      console.log("dapat session storage", sessionStorage.getItem('token')); //converts to json object
    } else {
      window.location.assign('http://10.54.36.49/landingPage/')
      console.log('key dose not exists');
    }
  }

  openLink(link: string) {
    window.open(link, '_blank');
  }
}





