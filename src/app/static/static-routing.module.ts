import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FeaturesComponent } from './features/features.component';
import { NodinComponent } from './nodin/nodin';
import { MomComponent } from './mom/mom';

const routes: Routes = [
  // { path: 'cacti', component: AboutComponent, data: { title: 'Quick Cacti' } },
  {
    path: 'nodin',
    component: NodinComponent,
    data: { title: 'NODIN' }
  },
  {
    path: 'mom',
    component: MomComponent,
    data: { title: 'NODIN' }
  },
  {
    path: 'tableList',
    component: FeaturesComponent,
    data: { title: 'TABLE LIST' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StaticRoutingModule {}
