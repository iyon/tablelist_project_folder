import {Component, Input, AfterViewInit, OnInit} from "@angular/core";
import { tembusan, listPerson, tembusan2 } from '../../data/data-pdf';
import { FeatureService } from "../provider/feature-service";

@Component({
  selector: 'nodin',
  templateUrl: './nodin.html',
  styleUrls: ['./nodin.css']  
})
export class NodinComponent implements OnInit {   
    tablePdf1:any = undefined;
    tablePdf2:any;
    tablePdf2Total;
    tablePdf3:any;
    tembusan:any;
    tembusan2:any;
    listPerson:any;

    monthNow;
    monthBefore;
    year;

    noNodin;
    kepada;
    dari;
    lampiran;
    perihal;

    constructor(private featureService: FeatureService) { 
        //month year
        this.monthBefore = "Oktober"       
        this.monthNow = "November"
        this.year = "2018"

        //header
        this.noNodin = "002/TC.02/NM-07/NL-11/NG-03/NJ-04/NL-01/NG-01/NJ-01/NM-01/VIII/2018"
        this.kepada = ["Vice President Financial Planning Analysis and Business Partner"]
        this.dari = [
            "Vice President Network Deployment and Services",
            "Vice President Network Planning and Engineering",
            "Vice President Network Operation Management",
            "Vice President Network Infrastructure Management",
            "General Manager Network Asset Management"
        ]
        this.lampiran = "1 berkas"
        this.perihal = "Pelaporan BTS On-air Telkomsel Juli 2018 (Periode 1- 31 Juli 2018)"        
         
        this.tembusan = [
            {"no":"1", "person":"Director of Network"},
            {"no":"2", "person":"Director of Finance"},
            {"no":"3", "person":"VP Network Planning & Engineering"},
            {"no":"4", "person":"VP Network Operation Management"},
            {"no":"5", "person":"VP Network Deployment and Services"},
            {"no":"6", "person":"GM IOC Management"},
            {"no":"7", "person":"GM RAN and Core Network Deployment"},
            {"no":"8", "person":"GM Network Integrated Program and Budgeting"}
        ];
        this.tembusan2 = [
            {"no":"1", "person":"Director of Network"},
            {"no":"2", "person":"Director of Finance"},
            {"no":"3", "person":"General Manager IOC Management"},
            {"no":"4", "person":"General Manager Network Integrated Program and Budgeting"}
        ];
        this.listPerson = [
            {"name": "Agus Witjaksono", "title":"Vice President Network Deployment and Services"},
            {"name": "Mustaghfirin", "title":"Vice President Network Planning and Engineering"},
            {"name": "Andrias Indra", "title":"Vice President Network Operation Management"},
            {"name": "Djoko S Kuntjoro", "title":"Vice President Network Infrastructure Management"},
            {"name": "Chairuddin1009", "title":"General Manager Network Asset Management"},
            {"name": "Cecep Riswanda", "title":"General Manager RAN and Core Network Deployment"},
            {"name": "Suharno", "title":"General Manager Radio Network Planning"},
            {"name": "Iswandi", "title":"General Manager RAN Operation"}
        ];        
    }

    ngOnInit(){
        //table data
        if(!undefined){
            this.featureService.NodinTable1().subscribe(data => this.tablePdf1 = data)
        }
        
        this.featureService.NodinTable2().subscribe(data => {
            let data1 = 0;
            let data2 = 0;
            let data3 = 0;
            let data4 = 0;
            let data5 = 0;
            
            data.forEach(x =>{
                data1 += x.data1
                data2 += x.data2
                data3 += x.data3
                data4 += x.data4
                data5 += x.data5
            })
            this.tablePdf2Total = {"data1": data1,"data2": data2,"data3": data3,"data4": data4,"data5": data5}
            console.log(this.tablePdf2Total,"totaal")
            this.tablePdf2 = data                      
        })    
        
    }

    //date to dateStr 'mm-yy'
    dateStr(dates){
        let listMonth = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
        let date = new Date(dates)
        let monthIdx = date.getMonth()  
        let year = date.getFullYear()
        let monthStr = listMonth[monthIdx]
        let year2Digit = year.toString().substring(2,4)
        let result = `${monthStr}-${year2Digit}`
        // console.log(`cek date: ${result}`)
        return result
    }

    valueToComma(params) {
        // if(params){
        //   console.log("bisaa")
        let a = params.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        // console.log(a, "aaa")
          return a
        // } else {
        //   console.log("tidak bisaa",params)
        // return 
        // };
    }
}