import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SharedModule } from '@app/shared';
import { StaticRoutingModule } from './static-routing.module';
import { FeaturesComponent } from './features/features.component';
import {AgTable} from './partial/ag-table';
import {AgGridModule} from 'ag-grid-angular';
import {FeatureService} from './provider/feature-service';
import { LukeComponent } from './modal/modalUpload';
import { ModalTable } from './partial/modal-tabel';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ModalFormComponent } from './modal-form/modal-form';
import { momTable } from './partial/mom-table';
import { ChildMessageRenderer } from './partial/child-message-renderer.component';
import { NodinComponent } from './nodin/nodin';
import { MomComponent } from './mom/mom';
import { ChildRendererMom } from './partial/child-renderer-mom.component';
import { Paragraph } from './partial/paragraph';

@NgModule({
  imports: [
    SharedModule, 
    StaticRoutingModule, 
    AgGridModule,
    AgGridModule.withComponents([ChildMessageRenderer, ChildRendererMom]),
    MDBBootstrapModule.forRoot(),
  ],
  declarations: [
    FeaturesComponent, 
    AgTable,
    ModalTable,
    LukeComponent,
    ModalFormComponent,
    momTable,
    ChildMessageRenderer,
    NodinComponent,
    MomComponent,
    ChildRendererMom,
    Paragraph
    // MDBBootstrapModule.forRoot(),
  ],
  providers: [
    FeatureService,
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class StaticModule {}
