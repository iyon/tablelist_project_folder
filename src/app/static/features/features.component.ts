import { Component, OnInit } from '@angular/core';
import { environment as env } from '@env/environment';
import { ROUTE_ANIMATIONS_ELEMENTS } from '@app/core';
import 'ag-grid-enterprise';
import {FeatureService} from '../provider/feature-service';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { statuss, newRoww } from '../partial/ag-table';

@Component({
  selector: 'anms-features',
  template: `
    <ag-table></ag-table>
    <mom-table></mom-table> 
  `,

  styleUrls: ['./features.component.scss']
})
export class FeaturesComponent implements OnInit {
  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
  versions = env.versions;
  constructor(private http: HttpClient, private featureService: FeatureService,public store: Store<any>) {

  }
  ngOnInit() {}

  openLink(link: string) {
    window.open(link, '_blank');
  }

  // updateStatus(){    
  //   this.featureService.changeStatus(statuss).subscribe(
  //     data => {        
  //       console.log("response", data)
  //       alert("submit success")
  //     },
  //     error => {
  //       alert("error")
  //     }
  //   )
  //   this.featureService.changeRow(newRoww).subscribe(
  //     data => {        
  //       console.log("response", data)
  //       alert("submit success")
  //     },
  //     error => {
  //       alert("error")
  //     }
  //   )
  // }

  
}
