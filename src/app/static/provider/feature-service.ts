import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { nodinComponent } from '../partial/nodin';

// import { Stock } from './stock-market.reducer';
import { MatDialog, MatNativeDateModule } from '@angular/material';
import { FileUpload, UpdateStat, UpdateRow } from '../partial/ag-table';
import { NodinUpload } from '../partial/modal-tabel';
const Server = 'http://10.54.36.49/api-btsonair/public'; // SERVER PAK EKI
// const Server = 'http://10.47.198.153/btsonair/public'; // local Abdur


@Injectable()
export class FeatureService  implements OnInit {
  private handleError: any;
  token: any;

  constructor(private   httpClient: HttpClient) {}

  public uploadFiles(files: FileUpload[]): Observable<any> {
    console.log('fileList', files)
    let formData: FormData = new FormData();
    let file: File
    files.forEach(element => {
        
        file = element.file
        formData.append(element.name, file, file.name)
    });
          return this.httpClient.post(Server + `/api/v1/upload`, formData)
            .pipe(map(res=>{
              console.log(res)
              return res
              
            }))
             
            
        
}

public nodinNe(files: FileUpload[], list: any): Observable<any>{
  let formData: FormData = new FormData();
  let listJson = JSON.stringify(list)
  console.log("list", listJson)
  formData.append("listJson", listJson)
  files.forEach(file => {
    formData.append("file", file.file)
  })
  
  
  return this.httpClient.post(Server + `/api/v1/upload`, formData)
  .pipe(map(res=>{
    // catchError(this.handleError('addHero', res)) 
    // catchError(this.handleError( res))
    console.log(res)
    return res
  }
  ))

  }




  public updateStat(stats: UpdateStat[])
  // : Observable<any> 
  {
    console.log('stat', stats);
    // let formData: FormData = new FormData();
    // let stat: string
    // stats.forEach(element => {
        
    //     stat = element.string
    //     formData.append(element.name, stat, stat.name)
    // });
    //       return this.httpClient.post(Server + `/api/v1/upload`, formData)
    //         .pipe(map(res=>{
    //           console.log(res)
    //           return res
              
    //         }))
             
            
        
}



  public changeStatus(list: UpdateStat[]){
  
    this.token = sessionStorage.getItem("token");
    var url = Server + "/api/v1/nodeb/update?token=" + this.token;
    return this.httpClient.post(url, list)
    .pipe(map(res=>{
    console.log("send",list);
    return res;
    })); 
  };

  public changeRow(list: UpdateRow[] ){

    this.token = sessionStorage.getItem("token");
    var url = Server + "/api/v1/nodeb/insert?token=" + this.token;
    return this.httpClient.post(url, list)
    .pipe(map(res=>{
    console.log("send",list);
      return res;
    })); 
  };

  public noTiket(tiket) {
    // "http://10.54.36.49/api-btsonair/public/api/table_list_mom?token="
    var url = Server + "/api/table_list_mom?token=" + tiket
    return this.httpClient.get(url)
    .pipe(map(res=>{
      return res;
    })); 
  }

  public listStatus(){
    var url = Server + "/api/v1/get_status";
    return this.httpClient.get(url)
    .pipe(map(res=>{
      return res;
    })); 
  };

  public getRowData(){
  
  }

  //START NODIN TABLE FEATURE
  public NodinTable1(){
    let str = "http://10.54.36.49/api-btsonair/public/api/bts_summary_1";
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }

  public NodinTable2(){
    let str = "http://10.54.36.49/api-btsonair/public/api/bts_summary_2";
    return this.httpClient.get<any[]>(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }

  public NodinTableDismantle(){
    // let str = "http://10.54.36.49/api-btsonair/public/api/bts_summary_3";
    let str = "http://10.54.36.49/api-btsonair/public/api/bts_summary_dismantle";
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }

  public NodinTableSummaryCheck(){
    // let str = "http://10.54.36.49/api-btsonair/public/api/bts_summary_4";
    let str = "http://10.54.36.49/api-btsonair/public/api/bts_summary_check";
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }

  public NodinTableNeedCheck(){
    let str = "http://10.54.36.49/api-btsonair/public/api/bts_summary_needcheck";
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }

  public NodinTableNeedCheck2G(){
    let str = "http://10.54.36.49/api-btsonair/public/api/bts_summary_needcheck_2g";
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }
  public NodinTableNeedCheck3G(){
    let str = "http://10.54.36.49/api-btsonair/public/api/bts_summary_needcheck_3g";
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }
  public NodinTableNeedCheck4G(){
    let str = "http://10.54.36.49/api-btsonair/public/api/bts_summary_needcheck_4g";
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }
  // END OF NODIN TABLE

  ngOnInit() {
    this.token = sessionStorage.getItem("token")
    console.log("token tiket", this.token)
    // if (sessionStorage.getItem('token')) {
    //   console.log("dapat session storage", sessionStorage.getItem('token')); //converts to json object
    // } else {
    //   window.location.assign('http://10.54.36.49/landingPage/')
    //   console.log('key dose not exists');
    // }
  }
}