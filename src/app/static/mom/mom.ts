import {Component, Input, OnInit} from "@angular/core";
import { FeatureService } from "../provider/feature-service";
import { AgTable } from "../partial/ag-table";


@Component({
  selector: 'mom',
  templateUrl: './mom.html',
  styleUrls: ['./mom.css']  
})
export class MomComponent implements OnInit {
  momTable1;
  momTable2;
  momTableDismantle;
  momTableSummaryCheck;
  momTableNeedCheck;
  momTableNeedCheck2G;
  momTableNeedCheck3G;
  momTableNeedCheck4G;
  noTiket;
  token;

    constructor(private featureService: FeatureService) {
      this.token = sessionStorage.getItem("token")
      this.featureService.noTiket(this.token).subscribe(data => {
        this.noTiket = data
      })

      this.featureService.NodinTable1().subscribe(data => {            
        this.momTable1 = data            
    })
      this.featureService.NodinTable2().subscribe(data => {            
        this.momTable2 = data            
    })
    this.featureService.NodinTableDismantle().subscribe(data => {            
      this.momTableDismantle = data            
    })
    this.featureService.NodinTableSummaryCheck().subscribe(data => {            
      this.momTableSummaryCheck = data            
    })  
    this.featureService.NodinTableNeedCheck().subscribe(data => {            
      this.momTableNeedCheck = data          
    })  

    this.featureService.NodinTableNeedCheck2G().subscribe(data => {            
      this.momTableNeedCheck2G = data          
    }) 
    this.featureService.NodinTableNeedCheck3G().subscribe(data => {            
      this.momTableNeedCheck3G = data          
    }) 
    this.featureService.NodinTableNeedCheck4G().subscribe(data => {            
      this.momTableNeedCheck4G = data          
    }) 
     
    }

    dateStr(dates){
      let listMonth = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
      let date = new Date(dates);
      // console.log("dates",dates)
      let monthNow = date.getMonth();
      // console.log("monthNow", monthNow)
      // let monthIdx = date.getMonth()-1;
      // console.log("monthIdx", monthIdx)  
      let year = date.getFullYear();
      // if ( monthNow = 0 )
      // { 
      //   year = year + 1;
      // }

      // if (monthIdx < 0 )
      // { 
      //   monthIdx = 11;
      //   year = year - 1;
      // }
      let monthNoww = listMonth[monthNow];
      // let monthStr = listMonth[monthIdx];
      let year2Digit = year.toString().substring(2,4)
      // let result = `${monthStr}-${year2Digit}`
      let resultt = `${monthNoww}-${year2Digit}`
      // console.log(`cek date: ${result}`)
      return resultt 
  }
  // TAMBAH IYON
  ngOnInit() {
    this.token = sessionStorage.getItem("token")
    console.log("token tiket", this.token)
    let listMonthLong = ["Januari", "Februari", "Maret", "April", "Mai", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"]      
      let dt = new Date();
      // console.log("dt", dt)
      let m = dt.getMonth()
      let n = dt.getMonth() - 1;
      let o = dt.getMonth() - 2;
      let currYearOld = dt.getFullYear();
      let currYear  = dt.getFullYear();
      let currYearNow = dt.getFullYear();
      if (n < 0)
      { 
        n = 11;
        currYear = currYear - 1;
      }
      if (o = -1){
        o = 11
        currYearOld = currYearOld - 1
      } else if (o = -2){
        o = 10
        currYearOld = currYearOld - 1
      }
      // console.log("currYear", currYear)
      // console.log("n nih", n)
      // document.getElementById("datetime").innerHTML = dt.toLocaleDateString();    
      document.getElementById("bulan").innerHTML = listMonthLong[n];
      document.getElementById("bulan2").innerHTML = listMonthLong[n];
      document.getElementById("bulan3").innerHTML = listMonthLong[n];
      document.getElementById("bulan4").innerHTML = listMonthLong[n];
      // document.getElementById("bulan5").innerHTML = listMonthLong[n];
      // document.getElementById("bulan6").innerHTML = listMonthLong[n];
      document.getElementById("bulan7").innerHTML = listMonthLong[m];
      // document.getElementById("bulan8").innerHTML = listMonthLong[m];
      document.getElementById("bulan9").innerHTML = listMonthLong[m];
      document.getElementById("bulan10").innerHTML = listMonthLong[o];
      document.getElementById("tahun").innerHTML = currYear.toString();
      document.getElementById("tahun2").innerHTML = currYear.toString();
      document.getElementById("tahun3").innerHTML = currYear.toString();
      document.getElementById("tahun4").innerHTML = currYear.toString();
      // document.getElementById("tahun5").innerHTML = currYear.toString();
      // document.getElementById("tahun6").innerHTML = currYear.toString();
      document.getElementById("tahun7").innerHTML = currYearNow.toString();
      // document.getElementById("tahun8").innerHTML = currYearNow.toString();
      document.getElementById("tahun9").innerHTML = currYearNow.toString();
      document.getElementById("tahun10").innerHTML = currYearOld.toString();
  }
}