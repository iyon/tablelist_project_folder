// Write a function:

// function solution(A);
// that, given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.

// For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.

// Given A = [1, 2, 3], the function should return 4.

// Given A = [−1, −3], the function should return 1.

// Assume that:

// N is an integer within the range [1..100,000]
// Each element of array A is an integer within the range [−1,000,000..1,000,000]
// Complexity:

// Expected worst-case time complexity is O(N)
// Expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments)
// Elements of input arrays can be modified.




function solution(A) {
    // write your code in JavaScript (Node.js 8.9.4)
    var swap = function(i, j) {
        var tmp = A[i];
        A[i] = A[j];
        A[j] = tmp;
    };
  
    for (let i = 0; i < A.length; i++) {
        while (0 < A[i] && A[i] - 1 < A.length
                && A[i] != i + 1
                && A[i] != A[A[i] - 1]) {
            swap(i, A[i] - 1);
        }
    }
  
    for (let i = 0; i < A.length; i++) {
        if (A[i] != i + 1) {
            return i + 1;
        }
    }
    return A.length + 1;

    
  }